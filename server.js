var http = require('http');
var express = require('express');
var app = express();
var logger = require('morgan');
var dataGenerator = require('./generateData.js');
var router = require('json-server').router(dataGenerator());
var config = require('./config');

app.use(logger('dev'));
app.use('/api', router);
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', './views')
app.use(require('stylus').middleware('public'));

// Our application will need to respond to health checks when running on
// Compute Engine with Managed Instance Groups.
app.get('/_ah/health', function(req, res) {
  res.status(200).send('ok');
});

app.get("/", (req, res) => {
  res.render('index');
})

var server = http.createServer(app);

server.listen(config.port);
server.on('error', err => {
  console.log(err);
});
server.on('listening', () => {
  console.log('listening on port: ', config.port);
})
